# Breezey project resources
## Maintainer:
Jonathan Alexander Sandoval López
Jr. Salesforce Developer

## 2021

This folder contains static resources for Breezey project, any modification or changes related to this repository should be addressed to the maintainer, personal email: jonathan.sand.1996@gmail.com
